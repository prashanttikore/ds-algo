package com.prashant.practice.binarySearchTree;

public class InsertionInBinarySearchTree {

    static class Node {
        int key;
        Node right;
        Node left;

        Node(int c) {
            key = c;
        }
    }

    public static void main(String[] args) {

    }

    static Node insertRecursive(Node root,int x){
        if(root == null)
            return new Node(x);
        else if(x < root.key)
            root.left = insertRecursive(root.left,x);
        else if(x > root.key)
            root.right = insertRecursive(root.right,x);
        return root;
    }

    static Node insert(Node root, int x) {
        Node node = new Node(x);
        Node lastNode = null;
        Node curr =root;
        while (curr != null) {
            lastNode = curr;

            if (curr.key == x)
                return root;
            else if (x < curr.key) {
                curr = curr.left;
            } else if (x > curr.key) {
                curr = curr.right;
            }

        }
        if(lastNode == null)
            return node;
        if (x < lastNode.key)
            lastNode.left = node;
        else
            lastNode.right = node;

        return root;

    }


}
