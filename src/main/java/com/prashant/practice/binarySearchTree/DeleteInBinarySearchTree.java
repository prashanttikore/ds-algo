package com.prashant.practice.binarySearchTree;

public class DeleteInBinarySearchTree {
    static class Node {
        int key;
        Node right;
        Node left;

    }

    static Node deleteRecursive(Node root,int x){
            if(root == null )
                return null;

            if(x < root.key)
                root.left = deleteRecursive(root.left,x);
            else if(x > root.key)
                root.right = deleteRecursive(root.right,x);
            else {
                if (root.left == null)
                    return root.right;
                else if (root.right == null)
                    return root.left;
                else {
                    Node succ = getLeftMostDecedent(root);
                    root.key = succ.key;
                    root.right = deleteRecursive(root.right, succ.key);

                }
            }
            return root;

    }


    private static Node getLeftMostDecedent(Node root) {
        root = root.right;
        while(root != null && root.left != null){
            root = root.left;
        }
        return root;

    }
}
