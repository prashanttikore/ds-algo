package com.prashant.practice.binarySearchTree;

public class CeilingInBST {

    static class Node{
        int key;
        Node left;
        Node right;

        public Node(int c){
            key = c;
        }
    }

    static Integer getCeiling(Node root, int x){
        Integer res=null;
        while(root != null){
            if(root.key == x)
                return root.key;
            else if(x < root.key){
                res = root.key;
                root = root.left;
            }else{
                root = root.right;
            }

        }
        return res;
    }

    public static void main(String[] args) {
        Node root=new Node(15);
        root.left=new Node(5);
        root.left.left=new Node(3);
        root.right=new Node(20);
        root.right.left=new Node(18);
        root.right.left.left=new Node(16);
        root.right.right=new Node(80);
        int x=17;

        System.out.print("Ceil: "+(getCeiling(root,17)));
    }
}
