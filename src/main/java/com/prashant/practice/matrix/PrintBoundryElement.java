package com.prashant.practice.matrix;

import java.util.Stack;

public class PrintBoundryElement {
    public static void main(String[] args) {
        int[][] arr = {{1,2,3,4},{5,6,7,8},{9,10,11,12},{13,14,15,16}};
        printBoundryElementsofArray(arr);
    }

    private static void printBoundryElementsofArray(int[][] arr) {
        Stack<Integer> stack = new Stack<>();
        for(int i=0;i<arr.length;i++){
            if(i==0) {
                for (int j = 0; j < arr[i].length; j++)
                    System.out.print( " "+arr[i][j]);
            } else if (i== arr.length-1){
                for(int j = arr.length-1;j>=0;j--)
                    System.out.print(" "+arr[i][j]);
            } else{
               stack.push(arr[i][0]);
                System.out.print(" "+arr[i][arr[i].length-1]);
            }

        }
       while(stack.empty() != true) {
            System.out.print(" "+stack.pop());
        }
    }


}
