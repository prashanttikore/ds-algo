package com.prashant.practice.string;

public class Subsequence {
    public static void main(String[] args) {
        String s1="ABCD",s2="AD",s3="ABCDE",s4="AED";
        System.out.println("s2 is subsequence of s1 ? = "+checksubsequence(s1,s2));
        System.out.println("s4 is subsequence of s3 ? = "+checksubsequence(s3,s4));
    }

    private static boolean checksubsequence(String str1, String str2) {
        int str2Index=0;
        for(int i=0;i<str1.length() && str2Index < str2.length();i++){
            if(str1.charAt(i) == str2.charAt(str2Index))
                str2Index++;

        }

        return str2Index == str2.length();
    }
}
