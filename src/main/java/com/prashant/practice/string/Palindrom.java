package com.prashant.practice.string;

public class Palindrom {
    public static void main(String[] args) {
        String s1="ABBBA";
        String s2="ASTRE";

        System.out.println("String s1 is "+checkPalindrome(s1));
        System.out.println("String s2 is "+checkPalindrome(s2));
    }

    private static String checkPalindrome(String str) {
        int start= 0;
        int end = str.length()-1;
        while(start<end){
            if(str.charAt(start) != str.charAt(end) )
                return "NON-PALINDROME";
            start++;
            end--;
        }
        return "PALINDROME";
    }
}
