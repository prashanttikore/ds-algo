package com.prashant.practice.sorting;
// Selection sort - Not stable
// O(n2)
public class SelectionSort {
    public static void main(String[] args) {
        int[] arr = {3,20,3,8,6,9,2,3};

        selectionSort(arr);
        for(int i:arr){

            System.out.print(" "+i);
        }
    }

    private static void selectionSort(int[] arr) {
        for(int i=0;i<arr.length;i++){
            int minIndex=i;
            for(int j=i+1;j<arr.length;j++){
                if(arr[j]<arr[minIndex])
                    minIndex=j;

            }
            swap(arr,i,minIndex);
        }
    }

    private static void swap(int[] arr, int i, int minIndex) {
        int temp = arr[i];
        arr[i] = arr[minIndex];
        arr[minIndex] = temp;

    }
}
