package com.prashant.practice.sorting;

public class UnionOfTwoSortedArray {
    public static void main(String[] args) {
        int[] a1= { 2,4,5,5,5,6,6,7};
        int[] a2 = {2,2,2,5,7,8,8,9,9,10};
        printUnion(a1,a2);
    }

    private static void printUnion(int[] a1, int[] a2) {
        int i=0;
        int j=0;

        while( i <a1.length && j< a2.length){
            if(i>0 && a1[i] == a1[i-1]) {
                i++;
                continue;
            }
            System.out.print(" "+a1[i]);
            i++;
            j++;

        }

        while(i <a1.length){
            if(i>0 && a1[i] == a1[i-1]) {
                i++;
                continue;
            }
            System.out.print(" "+a1[i]);
            i++;
        }
        while(j <a2.length){
            if(j>0 && a2[j] == a2[j-1]) {
                j++;
                continue;
            }
            System.out.print(" "+a2[j]);
            j++;
        }
        }



}
