package com.prashant.practice.sorting;

public class MergeFunction {
    public static void main(String[] args) {
        int[] arr ={10,15,20,11,30};
        int arr1[] = sortArray(arr,0,2,4);
        for (int a:arr1){
            System.out.print( " "+a);
        }
    }

    private static int[] sortArray(int[] arr, int start, int mid, int high) {

        int a1[]=new int[mid-start+1];
        int a2[]=new int[high-mid];

        for(int i=0;i<a1.length;i++)
            a1[i] = arr[start+i];

        for(int i=0;i<a2.length;i++)
            a2[i] =arr[mid+i+1];


       int i=0;
       int j=0;
       int k =start;
        while(i<a1.length && j<a2.length){
            if(a1[i] <= a2[j]){
                arr[k++]=a1[i++];
            }else{
                arr[k++]=a2[j++];
            }
        }
        while(i<a1.length)
            arr[k++] = a1[i++];
        while(j<a2.length)
            arr[k++] = a2[j++];
        return arr;
    }
}
