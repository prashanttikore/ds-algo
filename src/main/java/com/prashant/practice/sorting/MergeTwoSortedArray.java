package com.prashant.practice.sorting;

public class MergeTwoSortedArray {
    public static void main(String[] args) {
        int[] arr ={10,15,20};
        int[] arr1 = {5,6,6,15};

        mergeTwoArrayandPrint(arr,arr1);
    }

    private static void mergeTwoArrayandPrint(int[] arr, int[] arr1) {
        int arr3[] = new int[ arr.length + arr1.length];
        int i=0;
        int j=0;
        int k=0;
        while(i < arr.length && j < arr1.length){
            if (arr[i] <= arr1[j]){
                arr3[k] = arr[i];
                i++;
                k++;
            }else{
                arr3[k]=arr1[j];
                j++;
                k++;
            }
        }
        while(j<arr1.length)
            arr3[k++] = arr1[j++];
        while(i<arr.length)
            arr3[k++] = arr[i++];

        for(int a:arr3){
            System.out.print(" "+a);
        }
    }
}
