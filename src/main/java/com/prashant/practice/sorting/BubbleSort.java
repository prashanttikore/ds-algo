package com.prashant.practice.sorting;

/*

Stable Algorithms -> Bubble sort, Insertion sort, Merge sort...
Unstable Algorithms -> Selection sort, Heap sort, Quick sort...

Bubble sort- O(n2)

 */

public class BubbleSort {
    public static void main(String[] args) {
        int[] arr={10,8,20,5};
        bubbleSort(arr);
        for(int a:arr)
            System.out.print(" "+a);
    }

    private static void bubbleSort(int[] arr) {
        for(int i=0;i<arr.length-1;i++){
            boolean swapped = false;
            for(int j=0;j<arr.length-1-i;j++){
                if(arr[j] > arr[j+1]) {
                    swap(arr, j, j + 1);
                    swapped = true;
                }
            }

            if(swapped==false)
                break;
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;




    }


}
