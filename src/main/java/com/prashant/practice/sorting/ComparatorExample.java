package com.prashant.practice.sorting;

import java.util.Arrays;
import java.util.Comparator;

public class ComparatorExample {
    static class Point {
        int x;
        int y;

        Point(int x,int y){
            this.x=x;
            this.y=y;
        }
    }

    static class PointComparator implements Comparator<Point>{

        public int compare(Point o1, Point o2) {
            return o1.x-o2.x;
        }
    }

    public static void main(String[] args) {
        Point point1 = new Point(5,6);
        Point point2 = new Point(7,12);
        Point point3 = new Point(4,2);

        Point[] points = {point1,point2,point3};
        Arrays.sort(points,new PointComparator());
        for(Point p:points){
            System.out.println("["+p.x+","+p.y+"]");
        }
      //  Arrays.sort(points, Collections.reverseOrder());
      //  System.out.println("After reverse order : ");
       // for(Point p:points){
      //      System.out.println("["+p.x+","+p.y+"]");
       // }
    }

}
