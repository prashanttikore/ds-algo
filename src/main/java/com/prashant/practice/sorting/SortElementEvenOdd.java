package com.prashant.practice.sorting;

import java.util.Arrays;
import java.util.Comparator;

// sort the elements such that all even element comes first then all odd
public class SortElementEvenOdd {
    public static void main(String[] args) {
        Integer arr[] = {3,1,2,7,4,6,8,9,10};
        Arrays.sort(arr,new Mycmp());
        for(int a: arr)
        System.out.print(" "+a);
    }
    static class Mycmp implements Comparator<Integer>{

        @Override
        public int compare(Integer o1, Integer o2) {
            return o1 % 2 - o2 % 2;
        }
    }
}
