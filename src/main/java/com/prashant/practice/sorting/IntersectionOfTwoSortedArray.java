package com.prashant.practice.sorting;

public class IntersectionOfTwoSortedArray {
    public static void main(String[] args) {
        int[] a1= { 2,4,5,5,5,6,6,7};
        int[] a2 = {2,2,2,5,7};
        printIntersection(a1,a2);
    }

    private static void printIntersection(int[] a1, int[] a2) {
        int i=0;
        int j=0;

        while(i < a1.length && j < a2.length){
            if(i>0 && a1[i]==a1[i-1]) {
                i++;
                continue;
            }
            if(a1[i] < a2[j])
                i++;
            else if (a1[i] > a2[j])
                j++;
            else{
                System.out.print(" "+a1[i]);
                i++;
                j++;
            }

        }
    }
}
