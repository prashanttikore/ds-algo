package com.prashant.practice.sorting;

import java.util.Arrays;

/*

Arrays.sort()  used to sort primitive as well as non primitive
for primitive - internally dual pivot quicksort algo is used and It is not stable sort

for non Primitive internally merge sort is used which guaranties stability.
with non primitive we can use Collection.reverseOrder to reverse the order but cant use with primitives

 */
public class ArraysSortExample {

    public static void main(String[] args) {
        int[] arr = {1,4,2,6,9,7};
        Arrays.sort(arr);

        for (int i:arr) {
            System.out.print(" "+i);
        }

    }


}
