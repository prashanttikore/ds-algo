package com.prashant.practice.sorting;

import java.util.Arrays;
import java.util.Collections;

public class ComparableExample {
    static class Point implements Comparable<Point>{
        int x;
        int y;

        Point(int x,int y) {
            this.x = x;
            this.y = y;
        }

        public int compareTo(Point o) {
            return this.x-o.x;
        }
    }

    public static void main(String[] args) {
        Point point1 = new Point(5,6);
        Point point2 = new Point(7,12);
        Point point3 = new Point(4,2);

        Point[] points = {point1,point2,point3};
        Arrays.sort(points);
        for(Point p:points){
            System.out.println("["+p.x+","+p.y+"]");
        }
        Arrays.sort(points, Collections.<Point>reverseOrder());
        System.out.println("After reverse order : ");
        for(Point p:points){
            System.out.println("["+p.x+","+p.y+"]");
        }
    }


}
