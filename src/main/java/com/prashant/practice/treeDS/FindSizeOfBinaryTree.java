package com.prashant.practice.treeDS;

import java.util.LinkedList;
import java.util.Queue;

public class FindSizeOfBinaryTree {
    static class Node {
        int key;
        Node right;
        Node left;

        public Node(int x){
            int key = x;
        }

    }

    public static void main(String[] args) {
        Node node= new Node(10);
        node.right = new Node(30);
        node.left = new Node(20 );
        node.left.left = new Node(40);
        node.left.right = new Node(50);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);

        System.out.println(" Size of tree is : "+printNoOfNodesInBinaryTree(node));
    }

    private static int  printNoOfNodesInBinaryTree(Node node) {
        if(node == null)
            return 0;
       int count =0;
       Queue<Node> queue = new LinkedList<Node>();
       queue.add(node);
       count++;
       while(queue.isEmpty() == false){
           Node node1 = queue.poll();
           if(node1.right != null){
               queue.add(node1.right);
               count++;
           }
           if(node1.left != null){
               queue.add(node1.left);
               count++;
           }
       }
       return count;
    }
}
