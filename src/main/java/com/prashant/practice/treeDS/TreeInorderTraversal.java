package com.prashant.practice.treeDS;

public class TreeInorderTraversal {

    static class Node{
        int key;
        Node right;
        Node left;

        public Node(int x){
            key = x;
        }
    }

    public static void main(String[] args) {
        Node node = new Node(10);
        node.right = new Node(30);
        node.left = new Node(20 );
        node.left.left = new Node(40);
        node.left.right = new Node(50);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);

        printInorder(node);


    }

    private static void printInorder(Node node) {
        if (node != null) {
            printInorder(node.left);
            System.out.print(" "+node.key);
            printInorder(node.right);
        }
    }
}
