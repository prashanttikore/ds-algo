package com.prashant.practice.treeDS;

public class TreePostOrderTraversal {

    static class Node{
        int key;
        Node left;
        Node right;

        Node(int x){
            key = x;
        }

    }
    public static void main(String[] args) {
        Node node = new Node(10);
        node.right = new Node(30);
        node.left = new Node(20 );
        node.left.left = new Node(40);
        node.left.right = new Node(50);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);

        postOrderTraversal(node);
    }

    private static void postOrderTraversal(Node node) {
        if(node != null){
            postOrderTraversal(node.left);
            postOrderTraversal(node.right);
            System.out.print(" "+node.key);
        }
    }
}
