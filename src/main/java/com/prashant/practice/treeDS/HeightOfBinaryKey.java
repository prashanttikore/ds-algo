package com.prashant.practice.treeDS;

public class HeightOfBinaryKey {

    static class Node{
        int key;
        Node right;
        Node left;

    public Node(int x){
        this.key=x;
    }
    }

    public static void main(String[] args) {
        Node node= new Node(10);
        node.right = new Node(30);
        node.left = new Node(20 );
        node.left.left = new Node(40);
        node.left.right = new Node(50);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);
        System.out.println("Height Of Binary Tree "+findHeight(node));
    }

    private static int findHeight(Node node) {
        if(node==null)
            return 0;
        else
            return Math.max(findHeight(node.left),findHeight(node.right))+1;

    }


}
