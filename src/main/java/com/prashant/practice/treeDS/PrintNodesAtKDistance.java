package com.prashant.practice.treeDS;

public class PrintNodesAtKDistance {

    static class Node {
        int key;
        Node left;
        Node right;

        Node(int x){
            key = x;
        }
    }

    public static void main(String[] args) {

        Node node = new Node(10);
        node.right = new Node(30);
        node.left = new Node(20 );
        node.left.left = new Node(40);
        node.left.right = new Node(50);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);

        nodesAtKDistance(node,2);
    }

  private static void nodesAtKDistance(Node node,int k){
         int count = 0;
         if(node == null)
             System.out.println(0);
         else
             printKlevel(node,count,k);
  }

    private static void printKlevel(Node node, int count, int k) {
        if(node==null)
            return;
        if( count == k){
            System.out.print(" "+node.key);
        } else{
            count++;
            printKlevel(node.left, count, k);
            printKlevel(node.right, count, k);

        }
    }
}
