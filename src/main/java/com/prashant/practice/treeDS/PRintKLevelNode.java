package com.prashant.practice.treeDS;

public class PRintKLevelNode {

    static class Node{
        int key;
        Node right;
        Node left;

        Node(int k){
            this.key=k;
        }
    }

    public static void main(String[] args) {

        Node node = new Node(10);
        node.left = new Node(20);
        node.left.left = new Node(40);
        node.left.right = new Node(50);

        node.right = new Node(30);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);


        printNodeatLevelK(node,2);

    }

    private static void printNodeatLevelK(Node node, int k) {
        if(node == null)
           return ;
        if(k==0)
            System.out.println(" "+node.key);
         else {
            printNodeatLevelK(node.left, k - 1);
            printNodeatLevelK(node.right, k - 1);
        }

    }

}
