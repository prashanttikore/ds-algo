package com.prashant.practice.treeDS;

import java.util.Stack;

public class IterativeInorderTraversal {
    static class Node {
        int key;
        Node right;
        Node left;

        Node(int x){
            key = x;
        }

    }

    public static void main(String[] args) {
        Node node= new Node(10);
        node.right = new Node(30);
        node.left = new Node(20 );
        node.left.left = new Node(40);
        node.left.right = new Node(50);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);

        printIterativeInorder(node);
    }

    private static void printIterativeInorder(Node node) {
        if(node == null)
            return;
        Stack<Node> stack = new Stack<Node>();
        while(node.left != null){
                stack.add(node);
        }

    }
}
