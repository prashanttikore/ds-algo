package com.prashant.practice.treeDS;

import java.util.LinkedList;
import java.util.Queue;

public class LevelOrderTraversal {
    static class Node {
        int key;
        Node right;
        Node left;

        public Node(int x){
            this.key = x;
        }

        public static void main(String[] args) {

            Node node = new Node(10);
            node.right = new Node(30);
            node.left = new Node(20 );
            node.left.left = new Node(40);
            node.left.right = new Node(50);
            node.right.right = new Node(70);
            node.right.right.right = new Node(80);

            printLevelOrderTraversal(node);
        }

        private static void printLevelOrderTraversal(Node node) {

            if(node==null)
                return;
            Queue<Node> queue =new LinkedList();
            queue.add(node);
            while(queue.isEmpty()==false){
                Node node1 = queue.poll();
                System.out.print(" "+node1.key);
                if(node1.left != null)
                    queue.add(node1.left);
                if(node1.right != null )
                    queue.add((node1.right));
            }

        }
    }
}
