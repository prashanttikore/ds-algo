package com.prashant.practice.treeDS;

public class TreePreOrderTraversal {
    static class Node{
        int key;
        Node right;
        Node left;

        Node(int x){
            key  = x;
        }

    }

    public static void main(String[] args) {
        Node node = new Node(10);
        node.right = new Node(30);
        node.left = new Node(20 );
        node.left.left = new Node(40);
        node.left.right = new Node(50);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);
        preOrderTraversal(node);

    }

    private static void preOrderTraversal(Node node) {
        if(node != null){
            System.out.print(" "+node.key);
            preOrderTraversal(node.left);
            preOrderTraversal(node.right);
        }

    }


}
