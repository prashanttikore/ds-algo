package com.prashant.practice.treeDS;

public class FindingMaximumKeyValueFromBinaryKey {
    static class Node {
        int key;
        Node right;
        Node left;

        Node(int x){
            key = x;
        }

    }

    public static void main(String[] args) {
        Node node= new Node(10);
        node.right = new Node(30);
        node.left = new Node(20 );
        node.left.left = new Node(40);
        node.left.right = new Node(50);
        node.right.right = new Node(70);
        node.right.right.right = new Node(80);
        System.out.println("Maximum key is "+getmaximumKey(node));

    }

    static int getmaximumKey(Node node){
        if(node == null)
            return Integer.MIN_VALUE;
        else
            return Math.max(node.key,Math.max(getmaximumKey(node.left),getmaximumKey(node.right)));
    }
}
