package com.prashant.practice.heap;

public class HeapImpl {

    public static class MinHeap{
        int arr[];
        int size;
        int capacity;

        MinHeap(int c){
            capacity = c;
            size = 0;
            arr = new int[c];
        }

        int left(int i){
            return 2*i+1;
        }
        int right(int i){
            return 2*i+2;
        }
        int parent(int i){
            return (i-1)/2;
        }
    }

    public static void main(String args[]){
        MinHeap m1 = new MinHeap(8);
    }
}
