package com.prashant.practice.heap;

public class HeapInsert {

    public static class MinHeap{
        int size;
        int capacity;
        int[] arr;

        public MinHeap(int c){
            capacity = c;
            size = 0;
            arr = new int[c];
        }

        int left(int i){
            return (2*i)+1;
        }
        int right(int i){
            return (2*i)+2;
        }
        int parent(int i){
            return (i-1)/2;
        }

        public void insert(int c){
            if(size==capacity)
                return;
            size++;
            arr[size-1]=c;

            for(int i =(size-1);i !=0 && arr[parent(i)]>arr[i];){
                int temp = arr[parent(i)];
                arr[parent(i)] = arr[i];
                arr[i] = temp;
                i=parent(i);
            }
        }

    }

    public static void main(String[] args) {
        MinHeap minHeap = new MinHeap(11);
        minHeap.insert(3);
        minHeap.insert(2);
        minHeap.insert(15);
        minHeap.insert(20);
    }
}
