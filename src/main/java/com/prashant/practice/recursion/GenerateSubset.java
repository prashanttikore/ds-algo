package com.prashant.practice.recursion;

public class GenerateSubset {
    public static void main(String[] args) {
        String s1 = "ABC";
        generateSubset(s1,"",0);
    }

    private static void generateSubset(String s, String curr, int i) {
        if(i==s.length()){
            System.out.print(" "+curr);
            return;
        }
        generateSubset(s,curr,i+1);
        generateSubset(s,curr+s.charAt(i),i+1);
    }

}
