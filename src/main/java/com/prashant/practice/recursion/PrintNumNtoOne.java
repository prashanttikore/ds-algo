package com.prashant.practice.recursion;

public class PrintNumNtoOne {
    public static void main(String[] args) {
        printNumNto1(6);
    }

    private static void printNumNto1(int n) {
        if(n==0)
            return;
        System.out.print(" "+n);
        printNumNto1(n-1);
    }
}
