package com.prashant.practice.recursion;

public class TowerOfHanoi {
    public static void main(String[] args) {
         toh( 3, "A","C","B");
    }

    static void toh(int n,String from_rod,String to_rod,String aux_rod){
        if(n==1){
            System.out.println("Move disk 1 from rod "+from_rod+" to rod "+to_rod);
            return;
        }
        toh(n-1,from_rod,aux_rod,to_rod);
        System.out.println("Move disk "+n+" from rod "+from_rod+" to rod "+to_rod);
        toh(n-1,aux_rod,to_rod,from_rod);
    }
}
