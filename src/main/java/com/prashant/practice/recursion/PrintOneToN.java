package com.prashant.practice.recursion;

public class PrintOneToN {
    public static void main(String[] args) {
        printOnetoN(6,1);
    }

    private static void printOnetoN(int i,int count) {
        if(i==0)
            return;
        System.out.print(" "+count);
        printOnetoN(i-1,++count);
    }
}
