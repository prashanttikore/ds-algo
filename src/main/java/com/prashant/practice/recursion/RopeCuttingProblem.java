package com.prashant.practice.recursion;

public class RopeCuttingProblem {
    public static void main(String[] args) {
        int n=5;
        int a=2,b=5,c=1;

        int max= getMaxPieces(n,a,b,c);
        System.out.println("max pieces are "+ max);

        int n2=23;
        int a1=12,b1=9,c1=11;
        System.out.println("Max pieces are "+getMaxPieces(n2,a1,b1,c1));


    }

    private static int getMaxPieces(int n, int a, int b, int c) {
        if(n==0)
            return 0;
        if(n<0)
            return -1;
        int res = Math.max(getMaxPieces(n-a,a,b,c),Math.max(getMaxPieces(n-b,a,b,c),getMaxPieces(n-c,a,b,c)));

        if(res==-1)
            return -1;


        return  res+1 ;
    }
}
