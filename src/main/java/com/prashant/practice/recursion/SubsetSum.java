package com.prashant.practice.recursion;

import java.util.ArrayList;
import java.util.List;

public class SubsetSum {
    public static void main(String[] args) {
        int n=7;
        int arr[] = {1,2,3,4,5,6};
        List<List<Integer>> subsetList = new ArrayList<>();
         getAllSubset(arr,subsetList,0,new ArrayList<Integer>());
        for(List<Integer> subset :subsetList){
           Integer sum = subset.stream().reduce(0,Integer::sum);
           if (sum == n){
               System.out.println(subset);
           }
           /* for(int j:subset){
                System.out.print(" "+j);
            }*/
        }

    }

    private static void getAllSubset(int[] arr, List<List<Integer>> subsetList, int index,List<Integer> slist) {
        if(index==arr.length){
           subsetList.add(slist);
           return ;
        }
        getAllSubset(arr,subsetList,index+1,new ArrayList<>(slist));
        slist.add(arr[index]);
        getAllSubset(arr,subsetList,index+1,new ArrayList<>(slist));
    }
}
