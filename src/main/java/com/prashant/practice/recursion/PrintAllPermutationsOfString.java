package com.prashant.practice.recursion;

public class PrintAllPermutationsOfString {
    public static void main(String[] args) {
        String s="ABCD";
        printAllPerm(s,0);
    }

    private static void printAllPerm(String s, int index) {
        if(index==s.length()-1){
            System.out.print(" "+s);
            return;
        }
        for(int j=index;j<s.length();j++) {
            s=swap(s,index,j);
            printAllPerm(s,index+1);
            s= swap(s,index,j);
        }
    }

    private static String swap(String s, int index, int j) {
        char[] arr = s.toCharArray();
        char temp = arr[index];
        arr[index] = arr[j];
        arr[j]=temp;
       return String.valueOf(arr);
    }
}
