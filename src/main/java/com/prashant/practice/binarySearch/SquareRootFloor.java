package com.prashant.practice.binarySearch;

public class SquareRootFloor {
    public static void main(String[] args) {

        System.out.println("Floor square root of given no is: "+findSquareroot(38));
    }

    private static int findSquareroot(int k) {
       //return findbinarySquareRoot(k,1,k/2,1);

        int low=1;
        int high=k;
        int ans =-1;

        while(low<=high){
            int mid =(low+high)/2;
            int midSqr = mid*mid;
            if(midSqr==k)
                return mid;
            else if(midSqr > k)
                high = mid-1;
            else {
                low=mid+1;
                ans=mid;
            }
        }
        return ans;


    }
    /*private static  int findbinarySquareRoot(int k, int low,int high,int res){
        int mid = (low+high)/2;

        if(mid*mid < k) {
            res = mid;
            return findbinarySquareRoot(k, mid + 1, high,res);
        }
        else if(mid*mid > k)
            return  findbinarySquareRoot(k,low,mid-1,res);
        else
            res= mid;

        return res;
    }*/
}
