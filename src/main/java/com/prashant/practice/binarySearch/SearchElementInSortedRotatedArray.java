package com.prashant.practice.binarySearch;

public class SearchElementInSortedRotatedArray {
    public static void main(String[] args) {
        int[] arr = {10,20,30,40,50,8,9};
        System.out.println("Element is at index : "+find(arr,8));
        System.out.println("Element is at index : "+findIterativeBinay(arr,9));
    }

    private static int findIterativeBinay(int[] arr, int k) {
        int low=0;int high=arr.length-1;
        int x=0;
        while(low<=high){
            int mid= (low+high)/2;
            if(arr[mid] == k)
                return mid;
            else if(arr[mid] > arr[low]){
                if(k<arr[mid] && k >=arr[low])
                    high =mid-1;
                else
                    low=mid+1;
            }else{
                if(k>arr[mid] && k <= arr[high])
                    low=mid+1;
                else
                    high=mid-1;
            }
        }
        return -1;
    }

    private static int find(int[] arr, int k) {
        int low=0;
        int high=arr.length-1;

        int mid=(low+high)/2;
        if(arr[mid]==k)
            return mid;
        if(arr[mid] > arr[low] ) {
            if(arr[mid] >k && k>=arr[low])
              return binarySearch(arr, low, mid - 1,k);
            else
                return  binarySearch(arr, mid+1, high,k);
        }
        else {
            if(arr[mid] < k && arr[high]>=k)
                return  binarySearch(arr,mid+1,high,k);
            else
                return binarySearch(arr,low,mid-1,k);
        }


    }

    private static int binarySearch(int[] arr, int low, int high,int k) {
        if(low>high)
            return-1;
        int mid =(low+high)/2;

         if(arr[mid] > k)
            return binarySearch(arr,low,mid-1,k);
         else if (arr[mid] < k)
             return binarySearch(arr,mid+1,high,k);
          else
              return mid;

    }
}
