package com.prashant.practice.binarySearch;

public class BinarySearchRecursive {
    public static void main(String[] args) {
        int[] arr = {10,202,30,40,50};
        System.out.println("Element is present at index : "+search(arr,40));
    }

    private static int search(int[] arr, int k) {

        return binarySearchRec(arr,k,0,arr.length-1);
    }

    private static int binarySearchRec(int[] arr, int k, int low, int high) {
        if(low<high)
            return -1;
        int mid = (low+high)/2;
        if(arr[mid]==k){
            return mid;
        }
        if(arr[mid] > k){
            return binarySearchRec(arr,k,0,mid-1);
        } else
            return binarySearchRec(arr,k,mid+1,high);
    }
}
