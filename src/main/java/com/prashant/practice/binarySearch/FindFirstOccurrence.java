package com.prashant.practice.binarySearch;

public class FindFirstOccurrence {
    public static void main(String[] args) {
        int[] arr ={10,30,40,40,50,50,50,60};
        System.out.println("First occurrence of given element is at index : "+findFirstIndex(arr,40));
        System.out.println("First occurrence of given element is at index : "+findFirstIndex(arr,50));
    }

    private static int findFirstIndex(int[] arr, int k) {
        int low=0;
        int high = arr.length-1;

        while(low<=high){
            int mid = (low+high)/2;
            if(arr[mid] > k)
                high =mid-1;
            else if(arr[mid]<k)
                low=mid+1;
            else {
                if(mid==0 || arr[mid-1] != arr[mid])
                    return mid;
                else
                    high=mid-1;
            }
        }
        return -1;
    }
}
