package com.prashant.practice.binarySearch;

public class CountOccurrencesInSortedArray {
    // The idea is to find first occurrence and last occurrence of given no and then return  last occurrence minus first occurrence + 1
    public static void main(String[] args) {
        int arr[] = {10,20,20,20,30,30,30,30,40,50,60};
        System.out.println("total count of occurrences of given no is :"+findCount(arr,30));
        System.out.println("total count of occurrences of given no is :"+findCount(arr,20));
        System.out.println("total count of occurrences of given no is :"+findCount(arr,40));
    }

    private static int findCount(int[] arr, int k) {

        int firstOccurrenceIndex = getFirstIndex(arr,k);
        if(firstOccurrenceIndex == -1)
            return -1;
        else
        return getLastOccurrence(arr,k) -firstOccurrenceIndex +1;
    }

    private static int getLastOccurrence(int[] arr, int k) {
        int low=0;
        int high= arr.length-1;
        while(low<=high){
            int mid = (low+high)/2;
            if(arr[mid] > k )
                high = mid-1;
            else if(arr[mid] < k)
                low = mid+1;
            else{
                if(mid==arr.length-1 || arr[mid+1] != arr[mid])
                    return  mid;
                else
                    low = mid+1;
            }

        }
        return -1;
    }

    private static int getFirstIndex(int[] arr, int k) {
        int low =0;
        int high = arr.length-1;

        while(low<= high){
            int mid =(low+high)/2;

            if(arr[mid] > k){
                high = mid-1;
            }else if(arr[mid] < k)
                low = mid +1;
            else {
                if(mid==0 || arr[mid-1] != arr[mid])
                    return mid;
                else
                    high = mid-1;
            }
        }

        return -1;
    }


}
