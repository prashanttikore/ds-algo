package com.prashant.practice.binarySearch;

public class Count1sInSortedBinaryArray {
    // the idea is find first occurrence of 1 and then return length-first occurrence
    public static void main(String[] args) {
        int[] arr = {0,0,0,1,1,1,1,1};
        System.out.println("The count 1's in given binary array is : "+getCount(arr));
    }

    private static int getCount(int[] arr) {
        int firstIndex = getFirstIndexBinary(arr,0,arr.length-1);
        if(firstIndex==-1)
            return -1;
        else return arr.length-firstIndex;
    }



    private static int getFirstIndexBinary(int[] arr, int low, int high) {
        if(low>high)
            return -1;
        int mid = (low+high)/2;
        if(arr[mid] ==0)
          getFirstIndexBinary(arr,mid+1,high);
        else
        {
            if(mid==0 || arr[mid-1] != arr[mid])
                return mid;
            else
                getFirstIndexBinary(arr,low,mid-1);
        }

        return -1;
    }
}
