package com.prashant.practice.binarySearch;

public class MoveZerosToEnd {
    public static void main(String[] args) {
        int[] arr = {1,3,0,0,4,0,9};
        moveZeros(arr);
        for (int i :
                arr) {
            System.out.println(i);
        }
    }

    private static void moveZeros(int[] arr) {
        int i=0;
        int j=0;

        while(i<arr.length && j<arr.length){
            if(arr[i] !=0){
                swap(arr,i,j);
                i++;
                j++;
            }else{
                i++;
            }

        }
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }


}
