package com.prashant.practice.binarySearch;

public class FindLastOccurrence {
    public static void main(String[] args) {
        int arr[] = {10,10,20,20,20,30,40,50,50,50};
        System.out.println("Last occurrence of given no is at index "+findLastIndex(arr,50));
        System.out.println("Last occurrence of given no is at index "+findLastIndex(arr,20));
    }

    private static int findLastIndex(int[] arr, int k) {
       int low=0;
       int high = arr.length-1;
       while (low<=high){
           int mid = (low+high)/2;
           if(arr[mid] > k)
               high = mid-1;
           else if(arr[mid]<k)
               low= mid+1;
           else{
               if(mid==arr.length-1 || arr[mid+1] != arr[mid])
                   return mid;
               else
                   low = mid+1;
           }
       }
        return -1;
    }
}
