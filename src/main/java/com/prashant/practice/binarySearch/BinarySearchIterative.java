package com.prashant.practice.binarySearch;

public class BinarySearchIterative {
    public static void main(String[] args) {
            int[] arr={10,20,30,40,50,60};
        System.out.println("element present at index "+search(arr,40));
    }

    private static int search(int[] arr, int k) {

        int low = 0;
        int high = arr.length-1;


        while(low<=high){
            int mid= (low+high)/2;
            if(arr[mid]==k)
                return mid;
            else if(arr[mid] < k){
                low = mid+1;

            }else {
                high = mid-1;
            }

        }
        return -1;
    }
}
