package com.prashant.practice.graphDS;


import java.util.ArrayList;

public class DFSDisconnectedGraphImpl {

    public static void main(String[] args) {

        int v=5;
        ArrayList<ArrayList<Integer>> graph = new ArrayList<ArrayList<Integer>>(v);

        for(int i=0;i<v;i++)
            graph.add(new ArrayList<Integer>());

        addEdge(graph,0,1);
        addEdge(graph,0,2);
        addEdge(graph,1,2);
        addEdge(graph,3,4);

        boolean[] visited = new boolean[v+1];
        for(int i=0;i<v;i++){
            if(visited[i]==false)
                printDFS(graph,i,visited);
        }




    }

    private static void printDFS(ArrayList<ArrayList<Integer>> graph, int i, boolean[] visited) {
        visited[i]=true;
        System.out.print(" "+i);
        for(int l:graph.get(i)){
            if(visited[i]==false)
                printDFS(graph,l,visited);
        }
    }

    static void addEdge(ArrayList<ArrayList<Integer>> graph,int u,int v){
        graph.get(u).add(v);
        graph.get(v).add(u);
    }
}
