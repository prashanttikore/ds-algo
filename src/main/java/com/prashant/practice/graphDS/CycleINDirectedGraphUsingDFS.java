package com.prashant.practice.graphDS;

import java.util.ArrayList;

public class CycleINDirectedGraphUsingDFS {
    public static void main(String[] args) {
        int v1=4;
        ArrayList<ArrayList<Integer>> graph = new ArrayList(v1);
        for(int i=0;i<v1;i++){
            graph.add(i,new ArrayList<Integer>());
        }
        addEdge(graph,0,1);
        addEdge(graph,1,2);
        addEdge(graph,2,3);
        addEdge(graph,3,1);
        boolean[] visited = new boolean[v1+1];
        boolean[] rec = new boolean[v1+1];
        boolean result=false;
        for(int i=0;i<v1;i++){
            if(visited[i]==false) {
                rec[i]=true;
                result = printDFS(graph, i, visited,rec);
            }
        }
        System.out.println("Is Cycle Present ? "+ result);

        int v2=6;
        ArrayList<ArrayList<Integer>> graph2 = new ArrayList(v1);
        for(int i=0;i<v2;i++){
            graph2.add(i,new ArrayList<Integer>());
        }
        addEdge(graph2,0,1);
        addEdge(graph2,2,1);
        addEdge(graph2,2,3);
        addEdge(graph2,3,4);
        addEdge(graph2,4,5);
        addEdge(graph2,5,3);
        boolean[] visited2 = new boolean[v2+1];
        boolean[] rec2 = new boolean[v2+1];
        boolean result2 =false;
        for(int i=0;i<v2;i++){
            if(visited2[i]==false) {
                rec2[i]=true;
                result2 = printDFS(graph2, i, visited2,rec2);
            }
        }
        System.out.println("Is Cycle Present in graph2 ? "+ result2);

    }

    private static boolean printDFS(ArrayList<ArrayList<Integer>> graph, int i, boolean[] visited, boolean[] rec) {
        visited[i] = true;
        for (int l : graph.get(i)) {
            if (visited[l] == false) {
                rec[l] = true;
                if (printDFS(graph, l, visited, rec) == true)
                    return true;
            } else if (rec[l] == true)
                return true;
        }
        rec[i]=false;
        return false;

    }

    static void addEdge(ArrayList<ArrayList<Integer>> graph,int u, int v){
        graph.get(u).add(v);
    }
}
