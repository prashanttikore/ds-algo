package com.prashant.practice.graphDS;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class FindingConnectedComponantInGraph {
    public static void main(String[] args) {
        int v=9;
        ArrayList<ArrayList<Integer>> graph= new ArrayList();
        for(int i=0;i<v;i++)
            graph.add(new ArrayList<Integer>());

        addEdge(graph,0,1);
        addEdge(graph,0,2);
        addEdge(graph,1,2);

        addEdge(graph,3,4);

        addEdge(graph,5,6);
        addEdge(graph,5,7);
        addEdge(graph,7,8);

        boolean[] visited =  new boolean[v+1];
        int count=0;
        for(int i=0;i<v;i++){
            if(visited[i]==false) {
                visited[i] = true;
                count++;
                printBFS(graph,i,visited);
            }
        }
        System.out.println("disconnected component or island in graph are: "+count);

    }

    private static void printBFS(ArrayList<ArrayList<Integer>> graph, int i, boolean[] visited) {
        Queue<Integer> queue = new LinkedList<Integer>();
        queue.add(i);
        while (queue.isEmpty()==false){
            int val= queue.poll();
            for(int l : graph.get(val)){
                if(visited[l]==false){
                    visited[l]=true;
                    queue.add(l);
                }
            }
        }
    }

    static void addEdge(ArrayList<ArrayList<Integer>> graph,int u,int v){
        graph.get(u).add(v);
        graph.get(v).add(u);
    }
}
