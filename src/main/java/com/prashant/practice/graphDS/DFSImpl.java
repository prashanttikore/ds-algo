package com.prashant.practice.graphDS;


import java.util.ArrayList;

public class DFSImpl {
    public static void main(String[] args) {
        int v=7;
        ArrayList<ArrayList<Integer>> graph = new ArrayList();
        for(int i=0;i<v;i++)
            graph.add(new ArrayList<Integer>());
        addEdge(graph,0,1);
        addEdge(graph,0,4);
        addEdge(graph,1,2);
        addEdge(graph,2,3);
        addEdge(graph,4,5);
        addEdge(graph,4,6);
        addEdge(graph,5,6);

        boolean[] visited=new boolean[v+1];
        printBFS(graph,0,visited);

        System.out.println();
        //graph2

        int v2=6;
        ArrayList<ArrayList<Integer>> graph2 = new ArrayList();
        for(int i=0;i<v2;i++)
            graph2.add(new ArrayList<Integer>());
        addEdge(graph2,0,1);
        addEdge(graph2,0,2);
        addEdge(graph2,2,3);
        addEdge(graph2,1,3);
        addEdge(graph2,1,4);
        addEdge(graph2,4,5);

        boolean[] visited2=new boolean[v2+1];
        printBFS(graph2,0,visited2);

    }

    private static void printBFS(ArrayList<ArrayList<Integer>> graph, int s, boolean[] visited) {

            visited[s]=true;
            System.out.print(" "+s);

                for (int i : graph.get(s)){
                    if(visited[i]==false)
                    printBFS(graph,i,visited);
                }


    }

    static void addEdge(ArrayList<ArrayList<Integer>> list,int u,int v){

        list.get(u).add(v);
        list.get(v).add(u);
    }
}
