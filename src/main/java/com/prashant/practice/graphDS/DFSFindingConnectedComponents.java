package com.prashant.practice.graphDS;

import java.util.ArrayList;

public class DFSFindingConnectedComponents {
    public static void main(String[] args) {
        int v=7;
        ArrayList<ArrayList<Integer>> graph = new ArrayList<ArrayList<Integer>>(v);

        for(int i=0;i<v;i++)
            graph.add(new ArrayList<Integer>());
        addEdge(graph,0,1);
        addEdge(graph,0,2);
        addEdge(graph,1,2);
        addEdge(graph,3,4);
        addEdge(graph,5,6);

        boolean[] visited = new boolean[v+1];
        int count=0;
        for (int i=0;i<v;i++){
            if(visited[i] == false){
                printDFS(graph,i,visited);
                count++;

            }
        }
        System.out.println("Connected components are :"+count);


    }

    private static void printDFS(ArrayList<ArrayList<Integer>> graph, int i, boolean[] visited) {
        visited[i]=true;
        for(int l:graph.get(i)){
            if(visited[l]==false)
                printDFS(graph,l,visited);
        }
    }

    static void addEdge(ArrayList<ArrayList<Integer>> graph,int u,int v){
        graph.get(u).add(v);
        graph.get(v).add(u);
    }



}
