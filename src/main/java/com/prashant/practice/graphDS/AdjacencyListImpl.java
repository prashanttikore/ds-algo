package com.prashant.practice.graphDS;

import java.util.ArrayList;

public class AdjacencyListImpl {
    public static void main(String[] args) {
        int v=5;
        ArrayList<ArrayList<Integer>> adj = new ArrayList(v);// This is list representation of graph

        for (int i=0;i< v; i++)
            adj.add(new ArrayList<Integer>());

        addEdge(adj,0,1);
        addEdge(adj,0,2);
        addEdge(adj,1,2);
        addEdge(adj,1,3);

        printGraph(adj);


    }

    private static void printGraph(ArrayList<ArrayList<Integer>> adj) {
        for(ArrayList<Integer> l1:adj){
            for(Integer i:l1){
                System.out.print(i);
            }
            System.out.println();
        }
    }

    static void addEdge(ArrayList<ArrayList<Integer>> adj,int u,int v){
        adj.get(u).add(v);
        adj.get(v).add(u);
    }


}
