package com.prashant.practice.graphDS;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

public class BFSForDisconnectedGraph {
    public static void main(String[] args) {
        int v=7;
        ArrayList<ArrayList<Integer>> graph= new ArrayList<ArrayList<Integer>>();
        for(int i=0;i<v;i++)
            graph.add(new ArrayList<Integer>());
        addEdge(graph,0,1);
        addEdge(graph,0,2);
        addEdge(graph,1,3);
        addEdge(graph,2,3);
        addEdge(graph,4,5);
        addEdge(graph,4,6);
        addEdge(graph,5,6);

        boolean[] visited =new boolean[v+1];
        for(int i=0;i<v;i++){
            if(visited[i]==false)
                printBFS(graph,i,visited);
        }


    }

    private static void printBFS(ArrayList<ArrayList<Integer>> graph, int s, boolean[] visited) {
        Queue<Integer> queue = new LinkedList();
        queue.add(s);
        visited[s]=true;
        while(queue.isEmpty() == false){
            int val= queue.poll();
            System.out.print(" "+val);
            for(int l:graph.get(val)){
                if(visited[l]==false){
                    visited[l]=true;
                    queue.add(l);
                }
            }
        }

    }

    static void addEdge(ArrayList<ArrayList<Integer>> graph,int u,int v){
        graph.get(u).add(v);
        graph.get(v).add(u);
    }
}
