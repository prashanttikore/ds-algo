package com.prashant.practice.graphDS;

import java.util.ArrayList;

public class FindingCycleInGraphUsingDFS {
    public static void main(String[] args) {
        int v=4;
        ArrayList<ArrayList<Integer>> graph1 = new ArrayList<ArrayList<Integer>>(v);

        for(int i=0;i<v;i++)
            graph1.add(new ArrayList<Integer>());
        addEdge(graph1,0,1);
        addEdge(graph1,1,2);
        addEdge(graph1,1,3);
        addEdge(graph1,2,3);

        int v1=4;
        ArrayList<ArrayList<Integer>> graph2 = new ArrayList<ArrayList<Integer>>(v1);

        for(int i=0;i<v1;i++)
            graph2.add(new ArrayList<Integer>());
        addEdge(graph2,0,1);
        addEdge(graph2,0,3);
        addEdge(graph2,1,3);
        addEdge(graph2,1,2);
        addEdge(graph2,2,3);

        int v2=5;
        ArrayList<ArrayList<Integer>> graph3 = new ArrayList<ArrayList<Integer>>(v2);

        for(int i=0;i<v2;i++)
            graph3.add(new ArrayList<Integer>());
        addEdge(graph3,0,1);
        addEdge(graph3,1,4);
        addEdge(graph3,1,2);
        addEdge(graph3,2,3);

        System.out.println("is graph1 contains cycle? :"+checkIfGraphContainsCycle(graph1,v) );
        System.out.println("is graph2 contains cycle? :"+checkIfGraphContainsCycle(graph2,v1) );
        System.out.println("is graph3 contains cycle? :"+checkIfGraphContainsCycle(graph3,v2) );
    }

    private static boolean checkIfGraphContainsCycle(ArrayList<ArrayList<Integer>> graph, int v) {

        boolean[] visited=new boolean[v+1];
        for (int i=0;i<v;i++){
            if(visited[i]==false)
                if( printDFS(graph,i,visited,-1)==true)
                    return true;
        }
        return false;
    }

    private static boolean printDFS(ArrayList<ArrayList<Integer>> graph, int i, boolean[] visited, int parent) {
        visited[i]=true;
        for(int l:graph.get(i)){
            if(visited[l]==false){
                if( printDFS(graph,l,visited,i)==true)
                    return true;

            }else  if(l != parent)
                return true;

        }
        return false;
    }

    static void addEdge(ArrayList<ArrayList<Integer>> graph,int u,int v){
        graph.get(u).add(v);
        graph.get(v).add(u);
    }
}
