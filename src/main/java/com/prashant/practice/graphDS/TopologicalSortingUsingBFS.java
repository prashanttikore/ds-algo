package com.prashant.practice.graphDS;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;

public class TopologicalSortingUsingBFS {

    public static void main(String[] args) {
        int v = 5;
        HashMap<Integer, LinkedList<Integer>> graph = new HashMap<Integer, LinkedList<Integer>>(v);
        for (int i = 0; i < v; i++) {
            graph.put(i, new LinkedList<Integer>());
        }
        addEdge(graph, 0, 2);
        addEdge(graph, 0, 3);
        addEdge(graph, 1, 3);
        addEdge(graph, 1, 4);
        addEdge(graph, 2, 3);

        System.out.println("Following is topological sort of graph using BFS");
        printTopologicalSort(graph,v);

    }

    private static void printTopologicalSort(HashMap<Integer, LinkedList<Integer>> graph, int v) {
        int[] inDegree=new int[v+1];
        Queue<Integer> queue = new LinkedList();
        for(Map.Entry<Integer,LinkedList<Integer>> entry: graph.entrySet()){
            for(int i:entry.getValue()){
                inDegree[i]++;
            }
        }
        for(int i=0;i<v;i++){
            if(inDegree[i]==0)
                queue.add(i);
        }
        while(queue.isEmpty()==false){
            int val=queue.poll();
            System.out.print(" "+val);
            for(int i:graph.get(val)){
                if(--inDegree[i]==0)
                    queue.add(i);
            }
        }
    }

    private static void addEdge(HashMap<Integer, LinkedList<Integer>> graph, int v, int u) {
        graph.get(v).add(u);
    }
}
