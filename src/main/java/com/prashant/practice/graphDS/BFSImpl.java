package com.prashant.practice.graphDS;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;


public class BFSImpl {
    public static void main(String[] args) {

       //BFS print source vertices then print adjacent vertices of it

        //Creating 3 graph
        //graph1
        int v=5;
        ArrayList<ArrayList<Integer>> graph1 = new ArrayList();
        for(int i=0;i<v;i++)
            graph1.add(new ArrayList<Integer>());

        addEdge(graph1,0,1);
        addEdge(graph1,0,2);
        addEdge(graph1,2,3);
        addEdge(graph1,2,4);

        //graph2
        int v2=5;
        ArrayList<ArrayList<Integer>> graph2 = new ArrayList();
        for(int i=0;i<v2;i++)
            graph2.add(new ArrayList<Integer>());

        addEdge(graph2,0,1);
        addEdge(graph2,0,2);
        addEdge(graph2,1,2);
        addEdge(graph2,1,3);
        addEdge(graph2,2,3);

        //graph3
        int v3=7;
        ArrayList<ArrayList<Integer>> graph3 = new ArrayList();
        for(int i=0;i<v3;i++)
            graph3.add(new ArrayList<Integer>());

        addEdge(graph3,0,1);
        addEdge(graph3,0,2);
        addEdge(graph3,0,5);
        addEdge(graph3,1,3);
        addEdge(graph3,3,5);
        addEdge(graph3,2,4);
        addEdge(graph3,4,5);


        printBFS(graph1,v,0);
        System.out.println();

        printBFS(graph2,v2,0);
        System.out.println();

        printBFS(graph3,v3,0);


    }

    private static void printBFS(ArrayList<ArrayList<Integer>> graph1, int v, int s) {
        boolean[] visited=new boolean[v+1];
        Queue<Integer> queue = new LinkedList();
        queue.add(s);
        visited[s]=true;
        while(queue.isEmpty()== false){
            int val =queue.poll();
            System.out.print(" "+val);

            for(int l : graph1.get(val)){
                if(visited[l]==false){
                    visited[l]=true;
                    queue.add(l);
                }

            }

        }

    }

    private static void addEdge(ArrayList<ArrayList<Integer>> graph1, int u, int v) {
        graph1.get(u).add(v);
        graph1.get(v).add(u);
    }

}
