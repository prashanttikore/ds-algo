package com.prashant.practice.graphDS;

import java.util.ArrayList;
import java.util.Stack;


public class TopologicalSortUsingDFS {
    public static void main(String[] args) {
        int v=5;
        ArrayList<ArrayList<Integer>> graph = new ArrayList<ArrayList<Integer>>(v);
        for(int i=0;i<v;i++)
            graph.add(new ArrayList<Integer>());

        addEdge(graph,0,1);
        addEdge(graph,1,3);
        addEdge(graph,3,4);
        addEdge(graph,2,3);
        addEdge(graph,2,4);

        System.out.println("This is topological sort using DFS ");
        toplogicalsort(graph,v);

    }

    private static void toplogicalsort(ArrayList<ArrayList<Integer>> graph, int v) {
        boolean[] visited = new boolean[v+1];
        Stack stack = new Stack();
        for(int i=0;i<v;i++){
            if(visited[i]==false)
            printElements(graph,i,visited,stack);
        }
        while(stack.empty()==false){
            System.out.print(" "+stack.pop());
        }
    }

    private static void printElements(ArrayList<ArrayList<Integer>> graph, int i, boolean[] visited, Stack stack) {
        visited[i]=true;
        for(Integer l:graph.get(i)){
            if(visited[l]==false)
                printElements(graph,l,visited,stack);
        }

        stack.push(i);
    }



    static void addEdge(ArrayList<ArrayList<Integer>> graph, int v,int u){
        graph.get(v).add(u);
    }

}
