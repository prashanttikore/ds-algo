package com.prashant.practice.array;
/* Try more Inputs

findElement(array, length_of_array)


case1:
actual = findElement([5, 1, 4, 3, 6, 8, 10, 7, 9],9)
expected = 4

case2:
actual = findElement([6, 2, 5, 4, 7, 9, 11, 8, 10],9)
expected = 4

case3:
actual = findElement([5, 1, 4, 4],4)
expected = -1

*/
public class findElementLeftSidesmallRightsideGreater {
    public static void main(String[] args) {
        int arr[] = {5,1,4,3,6,8,10,7,9};
        System.out.println("The element in array where all elements of its left side are smaller than it and all element of right side is greater than it is "+  findElement(arr));
    }

    private static int findElement(int[] arr) {
        int[] lmax = new int[arr.length];
        int[] rmax = new int[arr.length];

      lmax[0] = Integer.MIN_VALUE;
      for(int i=1;i<arr.length;i++)
          lmax[i] = Math.max(lmax[i-1],arr[i-1]);
      rmax[arr.length-1] = Integer.MAX_VALUE;
      for (int i=arr.length-2;i>=0;i--){
          int rightMin = Math.min(arr[i+1],rmax[i+1]);
          if(lmax[i] < arr[i] && rightMin > arr[i])
              return arr[i];

          rmax[i] =rightMin;
      }
      return -1;
    }

}
