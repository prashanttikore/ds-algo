package com.prashant.practice.array;
// Maximum difference value of arr[j]-arr[i] such that j>i. j & i are indexes.
public class MaxDifferenceInArray {
    public static void main(String[] args) {
        int arr[] = {2,3,10,6,4,8,1 };
        printMaxrDifference(arr);
    }

    private static void printMaxrDifference(int[] arr) {
        int res = arr[1] - arr[0];
        int minValue = arr[0];
        for(int j=1;j<arr.length;j++){
            res= Math.max(res,arr[j]-minValue);
            minValue = Math.min(minValue,arr[j]);
        }
        System.out.println("Max diff "+ res);
    }
}
