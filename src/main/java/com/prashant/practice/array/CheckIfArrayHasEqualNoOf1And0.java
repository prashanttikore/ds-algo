package com.prashant.practice.array;

import java.util.HashMap;
import java.util.Map;

public class CheckIfArrayHasEqualNoOf1And0 {
    public static void main(String[] args) {
        int arr[] = { 1, 0, 0, 1, 0, 1, 1 };
        System.out.println("Subarray max length with equal no of 0 & 1 is :"+ getMaxLen(arr));
    }

    private static int getMaxLen(int[] arr) {
        int maxLen=0;
        int end=0;
        int start=0;

        for(int i=0;i<arr.length;i++){
            arr[i] =  arr[i]==0 ? -1 :1;
        }
        int sum=0;
        Map<Integer,Integer> hashMap=new HashMap<>();
        for(int i=0;i<arr.length;i++){
            sum += arr[i];
            if(sum==0){
                maxLen =i+1;
                end=i;
            }
            if(hashMap.containsKey(sum)){
                maxLen = Math.max(maxLen,i-hashMap.get(sum));
                end=i;
            }else{
                hashMap.put(sum,i);
            }
        }
        start = end-maxLen+1;
        System.out.println("Max subarray from index "+start+" to "+end);
        return maxLen;
    }
}
