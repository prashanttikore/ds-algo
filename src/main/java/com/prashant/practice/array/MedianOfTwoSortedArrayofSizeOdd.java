package com.prashant.practice.array;
// array must be sorted. Length of two must be equal, and length must be odd
public class MedianOfTwoSortedArrayofSizeOdd {
    public static void main(String[] args) {
        int[] arr1 = {1,6,8,10,12};
        int[] arr2 = {2,3,4,9,11};
        System.out.println("The median of these array is: "+findMedianOfTwoArray(arr1,0,arr1.length-1,arr2,0,arr2.length-1));
    }

    private static int findMedianOfTwoArray(int[] arr1, int start_f, int end_f, int[] arr2, int start_s, int end_s) {

        if(end_f-start_f+1 ==2 && end_s-start_s+1==2){
            int max = Math.max(arr1[start_f],arr2[start_s]);
            int min = Math.min(arr1[end_f],arr2[end_s]);
            return (max+min)/2 ;
        }
        int m1 = getMedian(arr1,start_f,end_f);
        int m2 = getMedian(arr2,start_s,end_s);

        int m1_med_index = (start_f + end_f)/2;
        int m2_med_index = (start_s + end_s)/2;

        if(m1>m2){
           return findMedianOfTwoArray(arr1,start_f,m1_med_index,arr2,m2_med_index,end_s);
        }else{
            return findMedianOfTwoArray(arr1,m1_med_index,end_f,arr2,start_s,m2_med_index);
        }
    }

    private static int getMedian(int[] arr, int start, int end) {
        int n = end -start +1;
        if(n%2==0){
            return  (arr[start + n/2] + arr[start + (n/2 -1)] )/2;
        }
        return arr[start + n/2];

    }
}
