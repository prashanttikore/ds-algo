package com.prashant.practice.array;

public class CheckEquilibrium {
    //if sum of element in left equal to sum of element in right then it's called equilibrium
    public static void main(String[] args) {
        int arr[] = {3,4,8,-9,20,6};
        System.out.println("Array has equilibrium point : "+checkEqPoint(arr) );
    }

    private static boolean checkEqPoint(int[] arr) {
        int totalSum=0;
        for(int i=0;i<arr.length;i++){
            totalSum +=arr[i];
        }
        int left_sum=0;
        for(int j=0;j<arr.length;j++){
            if(left_sum == totalSum-arr[j])
                return true;
            else {
                left_sum +=arr[j];
                totalSum = totalSum - arr[j];
            }
        }
        return false;



    }
}
