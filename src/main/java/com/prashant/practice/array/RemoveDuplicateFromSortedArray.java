package com.prashant.practice.array;

public class RemoveDuplicateFromSortedArray {
    public static void main(String[] args) {
        int[] arr = {10,10,20,20,30,30,30,40,50};
        int index = removeDuplicate(arr);
        for(int i=0;i<=index;i++){

            System.out.print(" "+arr[i]);
        }
    }

    private static int removeDuplicate(int[] arr) {
        int index = 0;
        if(arr.length==1)
            return 1;
        for(int i=1;i<arr.length;i++){
            if(arr[index] != arr[i]){
                index++;
                arr[index]=arr[i];
            }

        }
        return index;
    }

}
