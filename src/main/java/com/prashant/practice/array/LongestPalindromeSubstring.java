package com.prashant.practice.array;

public class LongestPalindromeSubstring {
    public static void main(String[] args) {
        System.out.println(longestPalindrome("cbbd"));
    }
    public static String longestPalindrome(String s) {
        char arr[] = s.toCharArray();
        String longest = ""+s.charAt(0);
        for(int i=0;i<arr.length;i++){
            for(int j=arr.length-1;j>=i;j--){
                if(isPalindrome(i,j,s)){
                    if(s.substring(i,j+1).length() > longest.length())
                        longest = s.substring(i,j+1);
                }
            }
            }
        return longest;
        }


    static  boolean  isPalindrome(int start,int end,String s){
        if(start==end)
            return true;

        while(start < end){
            if(s.charAt(start) != s.charAt(end))
                return false;
            start++;
            end--;
        }
        return true;
    }
}
