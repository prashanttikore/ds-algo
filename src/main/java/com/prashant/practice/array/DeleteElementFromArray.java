package com.prashant.practice.array;

import java.util.Arrays;

public class DeleteElementFromArray {

    public static void main(String[] args) {
        int[] arr = {23,34,67,45,46};
      Arrays.stream(deleteElement(arr,34)).forEach(System.out::println);
    }

    private static int[] deleteElement(int[] arr, int num) {
        int index = -1;
        for(int i=0;i<arr.length;i++){
            if(num==arr[i]) {
                index = i;
                break;
            }

        }
        for(int i=index;i<arr.length-1;i++){
            arr[i] =arr[i+1];
        }

        return arr;
    }
}
