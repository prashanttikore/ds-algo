package com.prashant.practice.array;

public class rotateArrayToLeftbyone {
    public static void main(String[] args) {
        int[] arr = {12,23,43,5,7,8};
        leftRotateByOne(arr);
        for (int i:arr ) {
            System.out.print(" "+i);
        }
    }

    private static void leftRotateByOne(int[] arr) {
        if(arr.length==1)
            return ;

        int first = arr[0];
         for(int i=0;i<arr.length-1;i++){
             arr[i] = arr[i+1];
         }
         arr[arr.length-1]=first;

    }
}

