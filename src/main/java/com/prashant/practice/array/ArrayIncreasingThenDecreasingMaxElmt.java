package com.prashant.practice.array;

import java.sql.Time;

public class ArrayIncreasingThenDecreasingMaxElmt {
    public static void main(String[] args) {
        int[] arr = { 12,34,56,78,124,1244,2134,354534,5345345,5345399,54680945};
        int max = findMax(arr,0,arr.length-1);
        long start = System.currentTimeMillis();
        System.out.println("Max element is:"+max);
        System.out.println(System.currentTimeMillis()-start);
        start = System.currentTimeMillis();
        System.out.println(findMax1(arr));
        System.out.println(System.currentTimeMillis()-start);
    }

    private static int findMax1(int[] arr) {
        int max = arr[0];
        for (int j : arr) {
            if (j > max) {
                max = j;
            }
        }
        return max;
    }

    private static int findMax(int[] arr,int start,int end) {
        if(start == end)
            return arr[start];
        if(end == start+1)
            return Math.max(arr[start],arr[end]);
        int median = (start+end)/2;
        if(arr[median-1] < arr[median] && arr[median+1] > arr[median]){
            return findMax(arr,median+1,end);
        } else if (arr[median-1] < arr[median] && arr[median+1] < arr[median])
            return arr[median];
        else
            return findMax(arr,start,median-1);
    }

}
