package com.prashant.practice.array;

public class FindingLeaderInArray {
    public static void main(String[] args) {
        int[] arr = {7,10,4,10,6,5,2};
        printLeadersInArray(arr);
    }

    private static void printLeadersInArray(int[] arr) {
        int cur_leader = arr[arr.length-1];
        System.out.print(" "+cur_leader);

        for(int i= arr.length-2 ;i>=0;i--){
            if(cur_leader < arr[i]){
                cur_leader = arr[i];
                System.out.print(" "+cur_leader);
            }
        }
    }
}
