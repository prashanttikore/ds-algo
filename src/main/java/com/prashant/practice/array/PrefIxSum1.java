package com.prashant.practice.array;

public class PrefIxSum1 {
    public static void main(String[] args) {
        int[] arr= {2,8,3,9,6,5,4};
        System.out.println("Sum between starting index: "+0+" to ending index:"+2+" is : "+getSum(0,2,arr));
        System.out.println("Sum between starting index: "+1+" to ending index:"+3+" is : "+getSum(1,3,arr));
        System.out.println("Sum between starting index: "+2+" to ending index:"+6+" is : "+getSum(2,6,arr));
    }

    private static int getSum(int l, int r,int[] arr) {
        int[] prefixarr= getPrefixSum(arr);
        if(l==0){
            return prefixarr[r];
        }
        return prefixarr[r]-prefixarr[l-1];
    }

    private static int[] getPrefixSum(int[] arr) {
        int[] prefixArr = new int[arr.length];
         int sum=0;
         int index=0;
        for(int i:arr){
            sum +=i;
            prefixArr[index++]=sum;
        }
        return prefixArr;
    }
}
