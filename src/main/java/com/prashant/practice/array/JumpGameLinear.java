package com.prashant.practice.array;

public class JumpGameLinear {
    public static void main(String[] args) {
        int[] arr =  {1,3,5,8,9,2,6,7,6,8,9};
        System.out.println("Minimum jump required to reach at the end of array is "+findJumps(arr));
    }

    private static int findJumps(int[] arr) {
        int a = arr[0];
        int b = arr[0];
        int jump =1;

        for(int i=1;i<arr.length;i++){

            if(i==arr.length-1){
                return jump;
            }
            a--;
            b--;
            if(arr[i]>b)
                b =arr[i];
            if(a==0){
                jump++;
                a = b;
            }

        }
        return jump;
    }
}
