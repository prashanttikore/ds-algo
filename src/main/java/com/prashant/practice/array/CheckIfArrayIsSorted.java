package com.prashant.practice.array;

public class CheckIfArrayIsSorted {
    public static void main(String[] args) {
        int[] arr = {12,34,56,7,87};
        int[] arr2 = {12,34,56,57,87};
        System.out.println(isArraySorted(arr));
        System.out.println(isArraySorted(arr2));
    }

    private static boolean isArraySorted(int[] arr) {
        if(arr.length ==1)
            return true;
        for(int i=1;i<arr.length;i++){
            if(arr[i] < arr[i-1])
                return false;
        }
        return true;
    }
}
