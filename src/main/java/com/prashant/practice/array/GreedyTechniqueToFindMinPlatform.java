package com.prashant.practice.array;

import java.util.Arrays;

public class GreedyTechniqueToFindMinPlatform {
    public static void main(String[] args) {
        double[] arrival =  {1.00,1.40,1.50,2.00,2.15,4.00};
        double[] departure= {1.10,3.00,2.20,2.30,3.15,6.00};

        System.out.println("Minimum platform required is: "+findPlatforms(arrival,departure) );
    }

    private static int findPlatforms(double[] arrival, double[] departure) {
        Arrays.sort(departure);
        int i=1,j=0,platform=1,maxplatform=1;
        while (i<arrival.length && j<departure.length){
            if(arrival[i] < departure[j]){
                platform++;
                maxplatform = Math.max(maxplatform,platform);
                i++;
            }else{
                platform--;
                j++;
            }
        }

        return maxplatform;
    }
}
