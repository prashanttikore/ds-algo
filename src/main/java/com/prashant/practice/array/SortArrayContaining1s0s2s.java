package com.prashant.practice.array;

public class SortArrayContaining1s0s2s {
    public static void main(String[] args) {
        int[] arr = {2,1,1,0,1,2,1,2,0,0,0,1};
        sortArray(arr);
        for (int i:arr) {
            System.out.print(" "+i);
        }

    }

    private static void sortArray(int[] arr) {
        int low=0;
        int high=arr.length-1;
        int mid=0;

        while(mid<=high){
            if(arr[mid]==0) {
                swap(arr, mid, low);
                mid++;
                low++;
            }
            else if(arr[mid]==2) {
                swap(arr, mid, high);
                high--;
            }
            else
                mid++;
        }
    }

    private static void swap(int[] arr,int i, int j) {
        int temp=arr[i];
        arr[i]=arr[j];
        arr[j]=temp;

    }
}
