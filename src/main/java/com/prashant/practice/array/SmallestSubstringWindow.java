package com.prashant.practice.array;
/*
Given two strings string1 and string2 , return the minimum window in string1 which will contain all
the characters in string2 in Time Complexity O(n) and Space Complexity O(1) Asked in :
 */
public class SmallestSubstringWindow {

    public static void main(String[] args) {
        String str = "ADOBECODEBANC";
        String pat = "ABC";
        System.out.print("Smallest window is :\n " +
                findMinWindow(str, pat));
    }

    private static String findMinWindow(String str, String pat) {
        int str_len = str.length();
        int ptr_len = pat.length();

        if(ptr_len > str_len){
            System.out.println("No window is present");
            return "";
        }
        int[] ascii_str = new int[256];
        int[] ascii_ptr = new int[256];

        for(int i=0;i<ptr_len;i++){
            ascii_ptr[pat.charAt(i)]++;
        }
        int count=0;
        int start =0;
        int length = Integer.MAX_VALUE;
        int start_index = -1;
        for(int j=0;j<str_len;j++){
            ascii_str[str.charAt(j)]++;
            if(ascii_ptr[str.charAt(j)] !=0 && ascii_ptr[str.charAt(j)] >= ascii_str[str.charAt(j)])
                count++;

            if(count==ptr_len){
                while (ascii_str[str.charAt(start)] > ascii_ptr[str.charAt(start)] || ascii_ptr[str.charAt(start)] == 0){
                    if (ascii_str[str.charAt(start)] > ascii_ptr[str.charAt(start)])
                        ascii_str[str.charAt(start)]--;
                    start++;

                    int len_window = j - start + 1;
                    if (length > len_window){
                        length = len_window;
                        start_index=start;
                    }
                }
            }
        }
        if (start_index == -1)
            	        {
                    System.out.println("window doesn't exists");
                    return "";
                    }
        return str.substring(start_index, start_index + length);
    }
}
