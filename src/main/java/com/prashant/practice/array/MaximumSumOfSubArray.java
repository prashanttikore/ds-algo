package com.prashant.practice.array;

public class MaximumSumOfSubArray {
    public static void main(String[] args) {
        int[] arr = {2,3,25,-8,7,-1,2,3};
        System.out.println("Max sub array sum is "+getEfficientSum(arr));
    }

    private static int getMaxSubarraySum(int[] arr) {
        int max=Integer.MIN_VALUE;
        int res=Integer.MIN_VALUE;
        for(int i=0;i<arr.length;i++){
            res =arr[i];
            for(int j=i+1;j<arr.length;j++){
                 res = res + arr[j];
                max = Math.max(max,res);
            }

        }
        return max;
    }

    private static int getEfficientSum(int[] arr){
        int  max= arr[0];
        int sum = arr[0];
        for(int i=1;i<arr.length;i++){
            sum +=arr[i];
            if(arr[i] > sum){
                max = arr[i];
                sum = arr[i];
            }else{
                max = Math.max(max,sum);
            }
        }
        return max;
    }
}
