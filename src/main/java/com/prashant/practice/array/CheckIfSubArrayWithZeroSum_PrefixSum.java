package com.prashant.practice.array;

import java.util.HashMap;

public class CheckIfSubArrayWithZeroSum_PrefixSum {
    public static void main(String[] args) {
        int[] arr ={ 1,2,4,-2,-2,5,-4,3};
        System.out.println("Is Array contains subarray with zero sum: "+checkIfSubArayWithZeroSum(arr));
    }

    private static boolean checkIfSubArayWithZeroSum(int[] arr) {
        int[] prefixSum=new int[arr.length];
        int sum=0;
        prefixSum[0]=arr[0];
        for (int i=1;i<arr.length;i++){
            prefixSum[i] = prefixSum[i-1]+arr[i];
        }
        HashMap<Integer,Integer> hashMap = new HashMap<>();
        for(int i=0;i<arr.length;i++){
            if(prefixSum[i]==0)
                return true;
            else if(hashMap.containsKey(prefixSum[i]))
                return  true;
            else
                hashMap.put(prefixSum[i],1);

        }
        return false;
    }
}
