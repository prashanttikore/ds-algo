package com.prashant.practice.array;

public class RainTappingProblem {
    public static void main(String[] args) {
        int[] arr = {2,0,2 };
        System.out.println("Water to be saved "+ getRainWaterEfficient(arr));
        int[] arr2 = {3,0,1,2,5 };
        System.out.println("Water to be saved "+ getRainWaterEfficient(arr2));


    }
//Naive

    private static int getRainTappingWater(int[] arr) {
        int res =0 ;
        int lmax = -1;
        int rmax = -1;
        for(int i=0;i<arr.length;i++){
            //finding lmax
            for(int j =0 ; j<=i;j++)
                lmax = Math.max(lmax,arr[j]);

            for(int k=i+1;k<arr.length;k++)
                rmax= Math.max(rmax,arr[k]);

            res = res + Math.min(lmax,rmax)-arr[i];

        }

        return res;

    }

    private static int getRainWaterEfficient(int[] arr){
        int[] lmax = new int[arr.length];
        int[] rmax = new int[arr.length];
        lmax[0]=arr[0];
        rmax[arr.length-1] = arr[arr.length-1];
        for(int i=1;i<arr.length;i++){
            lmax[i] = Math.max(lmax[i-1],arr[i]);
        }
        for (int j = arr.length-2;j>=0;j--){
            rmax[j]=Math.max(rmax[j+1],arr[j]);
        }
        int res=0;
        for(int k=1;k<arr.length-1;k++){
            res += Math.min(lmax[k],rmax[k])-arr[k];
        }
        return res;
    }
}
