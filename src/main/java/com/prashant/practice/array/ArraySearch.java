package com.prashant.practice.array;

public class ArraySearch {
    public static void main(String[] args) {
        //Array Increasing then decreasing
        int arr[] = {6,9,15,25,35,50,41,29,23,8};
        int max = findMax(arr);
        System.out.println(" Max element is "+max);
    }

    private static int findMax(int[] arr) {


        return binarySearch(arr,0,arr.length-1);
    }

    private static int binarySearch(int[] arr, int low, int high) {
        if(low==high)
            return arr[low];
        if(high-low==1){
            return Math.max(arr[high],arr[low]);
        }
        int mid = low + (high-low)/2;

        if(arr[mid]>arr[mid-1] && arr[mid] < arr[mid+1]){
         return binarySearch(arr,mid+1,high);
        } else if(arr[mid]>arr[mid-1] && arr[mid] > arr[mid+1])
            return arr[mid];
        else
           return binarySearch(arr,low,mid-1 );

    }
}
