package com.prashant.practice.array;

public class StockBuyAndSell {
    public static void main(String[] args) {
        int[] arr = {1,5,3,8,12};
        System.out.println("Max profit can be : "+getMaxProfit(arr));

    }

    private static int getMaxProfit(int[] arr) {
        int res=0;
        for(int i=1;i<arr.length;i++){
            if(arr[i-1] < arr[i])
                res = res + arr[i]-arr[i-1];

        }
        return res;
    }
}
