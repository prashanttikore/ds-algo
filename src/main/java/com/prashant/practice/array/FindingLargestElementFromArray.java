package com.prashant.practice.array;

public class FindingLargestElementFromArray {
    public static void main(String[] args) {
        int[] arr = {23, 34, 67, 45, 46};
        System.out.println(largestElement(arr));

    }

    private static int largestElement(int[] arr) {
        int max = arr[0];
        if(arr.length == 1)
            return max;

        for(int i= 1; i<arr.length;i++ ){
            if(arr[i] > max)
                max = arr[i];
        }

        return max;
    }
}
