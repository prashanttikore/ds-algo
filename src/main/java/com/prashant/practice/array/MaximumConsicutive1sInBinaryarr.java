package com.prashant.practice.array;

public class MaximumConsicutive1sInBinaryarr {

    public static void main(String[] args) {
        int[] arr = {0,1,1,0,0,1,1,1,1,0,0,1};

        System.out.println("Maximum Consecutive 1's are "+getMacConscOnes(arr));
    }

    private static int getMacConscOnes(int[] arr) {
        int res =0;
        int max = 0;

        for(int i=0;i<arr.length;i++){
            if(arr[i] ==1){
                res++;
            } else {
                max = Math.max(res,max);
                res=0;
            }
        }
        return max;
    }
}
