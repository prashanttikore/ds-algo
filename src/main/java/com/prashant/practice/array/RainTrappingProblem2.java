package com.prashant.practice.array;

public class RainTrappingProblem2 {
    public static void main(String[] args) {
        int[] barHeights = {5,3,4,6,3,6};
        System.out.println("Max no of water trapped in bars is: "+ findTrappedWater(barHeights));
    }

    private static int findTrappedWater(int[] barHeights) {
        int[] maxRight = new int[barHeights.length];
        int[] maxLeft = new int[barHeights.length];

        int lmax=barHeights[0];
        for(int i=1;i<barHeights.length;i++) {
            if (lmax < barHeights[i])
                lmax = barHeights[i];
            maxLeft[i] = lmax;
        }
        int rmax=barHeights[barHeights.length-1];
        for(int i=barHeights.length-1;i>=0;i--) {
            if (rmax < barHeights[i])
                rmax = barHeights[i];
            maxRight[i] = rmax;
        }
        int capacity = 0;
        for(int i=1;i<barHeights.length-1;i++){
            capacity += Math.min(maxLeft[i],maxRight[i])-barHeights[i];

        }
        return capacity;
    }
}
