package com.prashant.practice.array;

public class MoveZerosToEnd {
    public static void main(String[] args) {
        int[] arr = {1,0,0,0,2,4,5,7,0,0,6,40};
        int index = moveZerosatEnd(arr);
        for(int i =0; i<arr.length ; i++){
            System.out.print(" "+arr[i]);
        }
    }

    private static int moveZerosatEnd(int[] arr) {
        if(arr.length == 1 )
            return 0;
        int index = 0;
        for(int i=0;i<arr.length;i++){
            if(arr[i] != 0){

                arr[index] = arr[i];
                if(index != i)
                arr[i]=0;
                index++;
            }
        }
        return index;
    }
}
