package com.prashant.practice.array;

public class Sorting0s1s2s {

    public static void main(String[] args) {
        int[] arr={ 2,2,0,1,2,0,0,1,1,0,1,2,1,0};
        sortArray(arr);
        for(int i:arr){
            System.out.print(" "+i);
        }
    }

    private static void sortArray(int[] arr) {
        int left=0;
        int right=arr.length-1;
        int i=0;
        while(left<=right){

            if(arr[i] == 0) {
                arr[left++] = 0;
                i++;
            }
            if(arr[i]==2) {
                arr[right--] = 2;
                i++;
            }


        }
    }
}
