package com.prashant.practice.array;

public class FrequencyInSortedArray {
    public static void main(String[] args) {

int arr[] = { 10,10,10,25,30,30,40};
    printFrequency(arr);
    }

    private static void printFrequency(int[] arr) {
        int x = arr[0];
        int count =1;
        for(int i=1;i<arr.length;i++){
            if(x != arr[i]){
                System.out.println(x+" "+count);
                x=arr[i];
                count = 1;

            }else{
                count++;
            }
        }
        System.out.println(arr[arr.length-1]+" "+count);
    }
}
