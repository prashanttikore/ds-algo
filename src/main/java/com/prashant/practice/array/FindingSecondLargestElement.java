package com.prashant.practice.array;

public class FindingSecondLargestElement {
    public static void main(String[] args) {
        int[] arr = {12,34,56,7,87};
        int[] arr2 = {12,34,56,57,87};
        System.out.println(findSecondLargest(arr));
        System.out.println(findSecondLargest(arr2));
    }

    private static int findSecondLargest(int[] arr) {
        int largest = arr[0];
        if(arr.length == 1)
            return Integer.MIN_VALUE;
        int secondLargest = Integer.MIN_VALUE;

        for(int i=1 ; i <arr.length;i++ ){
            if(arr[i] > largest){
                secondLargest = largest;
                largest = arr[i];

            } else if(arr[i] > secondLargest){
                secondLargest = arr[i];
            }

        }
        return  secondLargest;
    }
}
