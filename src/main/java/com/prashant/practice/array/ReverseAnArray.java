package com.prashant.practice.array;

public class ReverseAnArray {
    public static void main(String[] args) {

        int[] arr = {12,23,4,5,3,65};
        reverse(arr);
        for(int i=0;i<arr.length;i++){
            System.out.print(" "+arr[i]);
        }
    }

    private static int[] reverse(int[] arr) {
        int i =0;
        int j = arr.length-1;
        while( i < j){
            int temp= arr[i];
             arr[i] = arr[j];
             arr[j] = temp;
             i++;
             j--;

         }
        return arr;
    }
}
